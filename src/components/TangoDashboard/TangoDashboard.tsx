import React from 'react';
import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import Statistics from '../Statistics/Statistics';
import CONFIG from '../../config.json';

const TangoDashboard = () => {
  const { t } = useTranslation();

  function renderTangoDeviceList() {
    return (
      <>
        <Typography m={1} variant="h5" component="div" sx={{ width: '80vw' }}>
          {t('tango.title')}
        </Typography>
        <Statistics tangoDB={CONFIG.TANGO_DB_URL} variables={undefined} />
      </>
    );
  }

  return <>{renderTangoDeviceList()}</>;
};

export default TangoDashboard;
