/* eslint-disable prefer-destructuring */
/* eslint-disable react/destructuring-assignment */
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Box, Card, CardContent, CardHeader, Grid, Typography } from '@mui/material';
// eslint-disable-next-line import/no-unresolved
import TangoAPI from '../services/api/tangoAPI';
import config from '../../config.json';

const WIDTH = 1200;
const WORKFLOW_INTERVAL_SECONDS = 1000;
const WORKFLOW_STATISTICS_INTERVAL_SECONDS = 1000;

const SPACING = 1;

function epochToDateString(timeInMilliseconds) {
  if (timeInMilliseconds === undefined || timeInMilliseconds === null) {
    return null;
  }
  return moment(0)
    .milliseconds(timeInMilliseconds * 1000)
    .format('Do MMM YYYY hh:mm:ss Z');
}

const Statistics = () => {
  const { t } = useTranslation();

  const PROCESSING_BLOCK_DATA = {
    time: {
      now: 0,
      last_update: 0,
      start: 0
    },
    processing_block: {
      state: 'initializing',
      scan_id: 0,
      time_since_last_payload: 0
    }
  };

  const PROCESSING_BLOCK_STATISTICS_DATA = {
    time: {
      now: 0,
      last_update: 0,
      start: 0
    },
    statistics: {
      ingestion_rate: 0,
      packet_count: 0,
      payloads_received: 0
    }
  };

  const RECEIVER_EVENTS_DATA = {
    type: 'receive_stats',
    time: 1670491530.743049,
    scan_id: 741333,
    state: 'stats',
    total_megabytes: 22017.6894,
    num_heaps: 66640,
    num_incomplete: 0,
    duration: 89242.64327836037,
    streams: [
      {
        id: 0,
        heaps: 16660,
        blocked: 0,
        incomplete_heaps: 0
      },
      {
        id: 1,
        heaps: 16660,
        blocked: 0,
        incomplete_heaps: 0
      }
    ]
  };

  const [processingBlockData, setProcessingBlockData] = useState(PROCESSING_BLOCK_DATA);
  const [processingBlockStatisticsData, setProcessingBlockStatisticsData] = useState(
    PROCESSING_BLOCK_STATISTICS_DATA
  );
  const [receiverEventsData, setReceiverEventsData] = useState(RECEIVER_EVENTS_DATA);
  const [counter, setCounter] = useState(0);
  const tangoDB = config.TANGO_DB_URL;
  const device = config.TANGO_DEVICE;

  useEffect(() => {
    async function retrieveProcessingBlockData() {
      await TangoAPI.executeCommands(tangoDB, device, 'processing_block')
        .then(data => {
          setProcessingBlockData(JSON.parse(data));
          setTimeout(retrieveProcessingBlockData, WORKFLOW_INTERVAL_SECONDS);
        })
        .catch(() => null);
    }

    async function retrieveProcessingBlockStatisticsData() {
      await TangoAPI.executeCommands(tangoDB, device, 'processing_block_statistics')
        .then(data => {
          setProcessingBlockStatisticsData(JSON.parse(data));
          setTimeout(retrieveProcessingBlockStatisticsData, WORKFLOW_STATISTICS_INTERVAL_SECONDS);
        })
        .catch(() => null);
    }

    async function retrieveReceiverEventData() {
      await TangoAPI.executeCommands(tangoDB, device, 'speed_latest_event')
        .then(data => {
          setReceiverEventsData(JSON.parse(data));
          setTimeout(retrieveReceiverEventData, WORKFLOW_STATISTICS_INTERVAL_SECONDS);
        })
        .catch(() => null);
    }

    if (counter === 0) {
      retrieveProcessingBlockData();
      retrieveProcessingBlockStatisticsData();
      retrieveReceiverEventData();
    }
    setCounter(1);
  }, [counter, device, tangoDB]);

  const displayValue = (label: string, value?: any, suffix?: string) => (
    <Grid container spacing={SPACING}>
      <Grid item xs={6}>
        <Typography paragraph>
          <b>{t(label)}</b>
        </Typography>
      </Grid>
      <Grid item xs={6}>
        <Typography paragraph>
          {value}
          <b>{suffix}</b>
        </Typography>
      </Grid>
    </Grid>
  );

  return (
    <>
      <Box m={1}>
        <Card variant="outlined" sx={{ minWidth: WIDTH }}>
          <CardHeader title={t('statistics.titleBasic')} />
          <CardContent sx={{ pt: '8px' }}>
            <div id="statistics-basics-Id" data-testid="statistics-basics-Id">
              {processingBlockData?.time && (
                <Grid m={0} container spacing={SPACING}>
                  <Grid item xs={6}>
                    {displayValue('statistics.time')}
                    {displayValue(
                      'statistics.lastRefresh',
                      epochToDateString(processingBlockData?.time?.now)
                    )}
                    {displayValue(
                      'statistics.lastUpdate',
                      epochToDateString(processingBlockData?.time?.last_update)
                    )}
                    {displayValue(
                      'statistics.start',
                      epochToDateString(processingBlockData?.time?.start)
                    )}
                  </Grid>
                  <Grid item xs={6}>
                    {displayValue('statistics.workflow')}
                    {displayValue('statistics.state', processingBlockData?.processing_block?.state)}
                    {displayValue(
                      'statistics.scanId',
                      processingBlockData?.processing_block?.scan_id
                    )}
                    {displayValue(
                      'statistics.timePayload',
                      processingBlockData?.processing_block?.time_since_last_payload
                    )}
                  </Grid>
                </Grid>
              )}
            </div>
          </CardContent>
        </Card>
      </Box>
      <Box m={1}>
        <Card variant="outlined" sx={{ minWidth: WIDTH }}>
          <CardHeader title={t('statistics.titleDetailed')} />
          <CardContent sx={{ pt: '8px' }}>
            <div id="statistics-detailed-Id" data-testid="statistics-detailed-Id">
              {processingBlockStatisticsData?.time && (
                <Grid m={0} container spacing={SPACING}>
                  <Grid item xs={6}>
                    {displayValue('statistics.time')}
                    {displayValue(
                      'statistics.lastRefresh',
                      epochToDateString(processingBlockStatisticsData?.time?.now)
                    )}
                    {displayValue(
                      'statistics.lastUpdate',
                      epochToDateString(processingBlockStatisticsData?.time?.last_update)
                    )}
                    {displayValue(
                      'statistics.start',
                      epochToDateString(processingBlockStatisticsData?.time?.start)
                    )}
                  </Grid>
                  <Grid item xs={6}>
                    {displayValue('statistics.statistics')}
                    {displayValue(
                      'statistics.ingestionRate',
                      Math.round(
                        (processingBlockStatisticsData?.statistics?.ingestion_rate || 0) * 100
                      ) / 100,
                      ' p/s'
                    )}
                    {displayValue(
                      'statistics.packetCount',
                      processingBlockStatisticsData?.statistics?.packet_count
                    )}
                    {displayValue(
                      'statistics.payloadsReceived',
                      processingBlockStatisticsData?.statistics?.payloads_received
                    )}
                  </Grid>
                </Grid>
              )}
            </div>
          </CardContent>
        </Card>
      </Box>
      <Box m={1}>
        <Card variant="outlined" sx={{ minWidth: WIDTH }}>
          <CardHeader title={t('statistics.titleReceiver')} />
          <CardContent sx={{ pt: '8px' }}>
            <div id="statistics-receiver-events" data-testid="statistics-receiver-events">
              {receiverEventsData?.time && (
                <Grid m={0} container spacing={SPACING}>
                  <Grid item xs={6}>
                    {displayValue(
                      'statistics.lastUpdate',
                      epochToDateString(receiverEventsData?.time)
                    )}
                    {displayValue('statistics.currentScan', receiverEventsData?.scan_id)}
                    {displayValue('')}
                    {displayValue(
                      'statistics.bestSite',
                      Math.round(receiverEventsData?.duration),
                      ' s'
                    )}
                  </Grid>
                  <Grid item xs={6}>
                    {displayValue(
                      'statistics.totalReceived',
                      Math.round((receiverEventsData?.total_megabytes || 0) * 100) / 100,
                      ' MB'
                    )}
                    {displayValue(
                      'statistics.currentSpeed',
                      Math.round(
                        ((receiverEventsData?.total_megabytes || 0) /
                          (receiverEventsData?.duration || 1)) *
                          100
                      ) / 100,
                      ' MB/s'
                    )}
                    {displayValue('statistics.numberHeaps', receiverEventsData?.num_heaps)}
                    {displayValue(
                      'statistics.numberHeapsIncomplete',
                      receiverEventsData?.num_incomplete
                    )}
                  </Grid>
                </Grid>
              )}
            </div>
          </CardContent>
        </Card>
      </Box>
    </>
  );
};
export default Statistics;
