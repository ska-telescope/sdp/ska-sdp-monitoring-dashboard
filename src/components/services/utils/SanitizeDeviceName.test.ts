import sanitizeDeviceName from './SanitizeDeviceName';

describe('test util Collapse Attribute function', () => {
  it('test if it returns the name without special characters', () => {
    expect(sanitizeDeviceName('test-device_name')).toEqual('test_device_name');
  });

  it('test if it returns the name without special characters', () => {
    expect(sanitizeDeviceName('test.device/name')).toEqual('test_device_name');
  });

  it('test if it returns the name without special characters', () => {
    expect(sanitizeDeviceName('test~device,name')).toEqual('test_device_name');
  });
});
