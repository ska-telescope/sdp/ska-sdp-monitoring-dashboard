/* eslint-disable no-restricted-syntax */
import { request as graphqlRequest } from 'graphql-request';
import {
  FETCH_ATTRIBUTES,
  FETCH_COMMANDS,
  FETCH_DEVICE_NAMES,
  FETCH_DEVICE_STATE,
  FETCH_ATTRIBUTE_METADATA,
  FETCH_ATTRIBUTES_VALUES,
  FETCH_DEVICE_METADATA,
  FETCH_DEVICE,
  FETCH_DATABASE_INFO,
  FETCH_ALL_CLASSES_WITH_DEVICES,
  FETCH_CLASS_WITH_DEVICES,
  FETCH_ALL_CLASSES,
  EXECUTE_COMMAND,
  FetchAttributes,
  FetchCommands,
  FetchDeviceNames
} from './graphqlQuery';

import sanitizeDeviceName from '../utils/SanitizeDeviceName';

import config from '../../../config.json';

function request<T = any>(tangoDB: string, query: string, args?: object): Promise<T> {
  const url = `${config.TANGO_DB_URL}`;
  return graphqlRequest(url, query, args || {});
}

export default {
  async fetchDeviceAttributes(tangoDB: string, device: string) {
    try {
      const data = await request<FetchAttributes>(tangoDB, FETCH_ATTRIBUTES, {
        device
      });
      const { attributes } = data.device;
      if (attributes != null) {
        return attributes;
      }
      // Some kind of error reporting could go here. This should only happen when the attributes resolver in the backend fails for some unexpected reason. For now, just return an empty list.
      return [];
    } catch (err) {
      return [];
    }
  },

  async fetchDatabaseInfo(tangoDB: string) {
    const data = await request(tangoDB, FETCH_DATABASE_INFO);
    return data.info;
  },

  async fetchCommands(tangoDB: string, device: string) {
    try {
      const data = await request<FetchCommands>(tangoDB, FETCH_COMMANDS, {
        device
      });
      return data.device.commands;
    } catch (err) {
      return [];
    }
  },

  async executeCommands(tangoDB: string, device: string, command: string) {
    const data = await request(tangoDB, EXECUTE_COMMAND, { device, command });
    // eslint-disable-next-line guard-for-in
    const { ok } = data.executeCommand;
    if (!ok) {
      return null;
    }
    const { output } = data.executeCommand;
    return output;
  },

  async fetchDeviceNames(tangoDB: string) {
    try {
      const data = await request<FetchDeviceNames>(tangoDB, FETCH_DEVICE_NAMES);
      return data.devices.map(({ name }) => name.toLowerCase());
    } catch (err) {
      return [];
    }
  },

  async fetchDevice(tangoDB: string, name: string) {
    const args = { name };

    // let device: any = null;
    const errors = [];

    try {
      const data = await request(tangoDB, FETCH_DEVICE, args);
      const [device] = data;
      if (device === null) {
        return null;
      }
      return { ...device, name: device.name.toLowerCase(), errors };
    } catch (err) {
      return null;
    }
  },

  async fetchAttributeMetadata(tangoDB: string, fullNames: string[]) {
    try {
      fullNames.forEach(fullName => {
        // eslint-disable-next-line no-unused-vars, no-param-reassign
        fullName = fullName.toLowerCase();
      });
      const data = await request(tangoDB, FETCH_ATTRIBUTE_METADATA, {
        fullNames
      });
      const result = {};

      for (const attribute of data.attributes) {
        const {
          device,
          name,
          dataformat,
          datatype,
          unit,
          enumLabels,
          label,
          maxalarm,
          minalarm,
          minvalue,
          maxvalue
        } = attribute;

        const fullName = `${device}/${name}`;
        const dataFormat = dataformat.toLowerCase();
        const dataType = datatype;
        const enumlabels = enumLabels;

        result[fullName] = {
          dataFormat,
          dataType,
          unit,
          enumlabels,
          label,
          maxAlarm: maxalarm,
          minAlarm: minalarm,
          minValue: minvalue,
          maxValue: maxvalue
        };
      }

      return result;
    } catch (error) {
      const errors: string[] = [];
      error.response.errors.map(err => errors.push(err.desc));
      throw errors.join(' ');
    }
  },

  async fetchDeviceState(tangoDB: string, name: string) {
    try {
      const args = { name };
      const data = await request(tangoDB, FETCH_DEVICE_STATE, args);
      return data.device.state;
    } catch (err) {
      return null;
    }
  },

  async fetchAttributesValues(
    tangoDB: string,
    fullNames: string[]
  ): Promise<
    Array<{
      name: string;
      device: string;
      value: any;
      writevalue: any;
      timestamp: number;
    }>
  > {
    try {
      const data = await request(tangoDB, FETCH_ATTRIBUTES_VALUES, {
        fullNames
      });
      return data.attributes;
    } catch (err) {
      return [];
    }
  },

  async fetchAllClassesAndDevices(tangoDB: string) {
    try {
      const data = await request(tangoDB, FETCH_ALL_CLASSES_WITH_DEVICES);
      return data.classes;
    } catch (err) {
      return [];
    }
  },

  async fetchClassAndDevices(tangoDB: string, name: string) {
    try {
      const args = { name };
      const data = await request(tangoDB, FETCH_CLASS_WITH_DEVICES, args);
      return data.classes;
    } catch (err) {
      return [];
    }
  },

  async fetchAllClasses(tangoDB: string) {
    try {
      const data = await request(tangoDB, FETCH_ALL_CLASSES);
      return data.classes;
    } catch (err) {
      return [];
    }
  },

  async fetchSelectedClassesAndDevices(tangoDB: string, variableNames) {
    if (variableNames?.length === 0) return [];
    let data: any;
    try {
      let query = '{';
      for (const variable of variableNames) {
        query += `
            ${variable.class}: classes(pattern: "${variable.class}") {
              name
              devices {
                name,
                exported,
                connected
              }
            }
          `;
      }
      query += '}';
      data = await request(tangoDB, query);
    } catch (err) {
      return [];
    }

    return data.classes;
  },

  async fetchDevicesProperty(tangoDB: string, variableNames) {
    let data: any;
    try {
      let query = '{';
      for (const variable of variableNames) {
        query += `
            ${sanitizeDeviceName(variable.device)}: device(name: "${variable.device}") {
              name,
              exported,
              connected
            }
          `;
      }
      query += '}';
      data = await request(tangoDB, query);
    } catch (err) {
      return [];
    }

    return Object.values(data);
  },

  async fetchDeviceMetadata(tangoDB: string, deviceNames: string[]) {
    const result = {};

    for (const deviceName of deviceNames) {
      let data: any;
      try {
        // eslint-disable-next-line no-await-in-loop
        data = await request(tangoDB, FETCH_DEVICE_METADATA, { deviceName });
      } catch (err) {
        return null;
      }
      if (data.device) {
        const { alias } = data.device;
        result[deviceName] = { alias };
      }
    }

    return result;
  },
  async fetchDevicesMetadata(tangoDB: string, deviceNames: string[]) {
    let data: any;
    try {
      let query = '{';
      for (const device of deviceNames) {
        query += `
            ${sanitizeDeviceName(device)}: device(name: "${device}") {
              name, alias
            }
          `;
      }
      query += '}';
      data = await request(tangoDB, query);
    } catch (err) {
      return null;
    }

    return Object.values(data);
  }
};
