import React from 'react';

import { createTheme, CssBaseline, Grid, ThemeProvider } from '@mui/material';

import TangoDashboard from '../TangoDashboard/TangoDashboard';

// Eventually we'll be able to pull in the colors from a standard library
const theme = createTheme({
  palette: {
    primary: {
      light: '#c1c6ca',
      main: '#c1c6ca',
      dark: '#212121',
      contrastText: '#000000'
    },
    secondary: {
      main: '#070068',
      light: '#070068',
      dark: '#f7be00',
      contrastText: '#FFFFFF'
    },
    error: {
      main: '#F44336',
      light: '#E57373',
      dark: '#D32F2F',
      contrastText: '#FFFFFF'
    },
    success: {
      main: '#66BB6A',
      light: '#81C784',
      dark: '#388E3C',
      contrastText: '##000000'
    }
  },
  shape: {
    borderRadius: 10
  },
  typography: {
    fontFamily: 'Roboto, Helvetica, Arial, sans-serif'
  },
  components: {
    MuiCard: {
      styleOverrides: {
        root: {
          borderWidth: '1px',
          borderStyle: 'solid',
          borderColor: '#c1c6ca'
        }
      }
    }
  }
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline enableColorScheme />
      <Grid container spacing={2}>
        <Grid item md={2}>
          <TangoDashboard />
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}

export default App;
