## The following should be standard includes
# include core makefile targets for release management
-include .make/base.mk
-include .make/oci.mk
-include .make/k8s.mk
-include .make/helm.mk

K8S_CHART := ska-sdp-monitoring-dashboard
HELM_CHARTS_TO_PUBLISH := ska-sdp-monitoring-dashboard
